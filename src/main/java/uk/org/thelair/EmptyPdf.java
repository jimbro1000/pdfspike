package uk.org.thelair;

import static uk.org.thelair.App.currentDate;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;

/*default*/ class EmptyPdf {

  private EmptyPdf() {
    /* does nothing except hide the default constructor */
  }

  /*default*/
  static PDDocument createPdf() {
    PDDocument pdDocument = new PDDocument();
    PDDocumentInformation pdDocumentInformation = pdDocument
        .getDocumentInformation();
    pdDocumentInformation.setCreationDate(currentDate());
    return pdDocument;
  }

}
