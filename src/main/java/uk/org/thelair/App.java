package uk.org.thelair;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

public class App {

  private static Logger logger = Logger.getLogger(App.class);

  public static void main(String[] args) {
    App app = new App();
    try {
      app.pdfApp(args);
    } catch (IOException e) {
      logger.error("IOException");
    }
  }

  /*default*/
  static Calendar currentDate() {
    Calendar calendar = new GregorianCalendar();
    Date date = new Date();
    calendar.setTime(date);
    return calendar;
  }

  private void pdfApp(String[] args) throws IOException {
    String folder = ".";
    if (args.length > 0) {
      folder = args[0];
    }
    makeEmptyPdf(folder);
    File file = new File(folder + "/empty.pdf");
    PDDocument pdDocument = PDDocument.load(file);
    pdDocument.addPage(EmptyPage.createPage());
    pdDocument.addPage(EmptyPage.createPage());
    pdDocument.removePage(0);
    addTextToPage(0, pdDocument,
        "This is text on the first page, it is long enough to wrap onto a second line without exceeding the page boundary",
        25, 500);
    addTextToPage(1, pdDocument, "This text goes on page 2", 50, 200);
    setProperties(pdDocument);
    pdDocument.save(folder + "/pages.pdf");
    pdDocument.close();
  }

  private void makeEmptyPdf(String folder) throws IOException {
    try (PDDocument pdDocument = EmptyPdf.createPdf()) {
      pdDocument.addPage(EmptyPage.createPage());
      pdDocument.save(folder + "/empty.pdf");
    }
  }

  private void setProperties(PDDocument pdDocument) {
    PDDocumentInformation pdDocumentInformation = pdDocument
        .getDocumentInformation();
    pdDocumentInformation.setAuthor("Julian Brown");
    pdDocumentInformation.setTitle("PDFBox spike");
    pdDocumentInformation.setCreator("PDFBox App");
    pdDocumentInformation.setSubject("Example Document");
    pdDocumentInformation.setModificationDate(currentDate());
  }

  private void addTextToPage(final int pageIndex, final PDDocument pdDocument,
      final String text, final int startX, final int startY)
      throws IOException {
    PDPage page = pdDocument.getPage(pageIndex);
    addTextToPage(pdDocument, page, text, startX, startY);
  }

  private void addTextToPage(final PDDocument pdDocument, final PDPage page,
      final String text, final int startX, final int startY)
      throws IOException {
    PDPageContentStream contentStream = new PDPageContentStream(pdDocument,
        page);
    PDFont pdFont = PDType1Font.TIMES_ROMAN;
    final float fontSize = 14;
    final float margin = 72;
    final float leading = 1.5f * fontSize;
    PDRectangle mediaBox = page.getMediaBox();
    List<String> flowedText = reflowText(text, mediaBox, pdFont, fontSize,
        margin);
    contentStream.beginText();
    contentStream.setFont(pdFont, fontSize);
    contentStream.newLineAtOffset(startX, startY);
    for (String line : flowedText) {
      contentStream.showText(line);
      contentStream.newLineAtOffset(0, -leading);
    }
    contentStream.newLine();
    contentStream.endText();
    contentStream.close();
  }

  private List<String> reflowText(final String content, final PDRectangle box,
      final PDFont pdFont, final float fontSize, final float margin)
      throws IOException {
    String text = content;
    List<String> result = new ArrayList<>();
    int lastSpace = -1;
    float width = box.getWidth() - 2 * margin;
    while (text.length() > 0) {
      int spaceIndex = text.indexOf(' ', lastSpace + 1);
      if (spaceIndex < 0) {
        spaceIndex = text.length();
      }
      String phrase = text.substring(0, spaceIndex);
      float size = fontSize * pdFont.getStringWidth(phrase) / 1000;
      if (size > width) {
        if (lastSpace < 0) {
          lastSpace = spaceIndex;
        }
        phrase = text.substring(0, lastSpace);
        result.add(phrase);
        text = text.substring(lastSpace).trim();
        lastSpace = -1;
      } else if (spaceIndex == text.length()) {
        result.add(text);
        text = "";
      } else {
        lastSpace = spaceIndex;
      }
    }
    return result;
  }
}
