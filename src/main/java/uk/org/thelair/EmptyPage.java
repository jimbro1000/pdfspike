package uk.org.thelair;

import org.apache.pdfbox.pdmodel.PDPage;

/*default*/ class EmptyPage {

  private EmptyPage() {
    /*hide default constructor*/
  }

  /*default*/
  static PDPage createPage() {
    return new PDPage();
  }
}
